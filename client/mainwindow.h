#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QTcpSocket"
#include "QTimer"
#include "QBuffer"
#include <QTreeWidget>
#define ONE_MIN 60000

const QByteArray request = "REQUEST TO SERVER";
const QByteArray answer = "CLIENT RECIEVED DATA FROM SERVER";

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_connectButton_clicked();
    void sendRequest();
    void readData();

signals:

private:
    Ui::MainWindow *ui;
    QTcpSocket* client_socket;
    QTimer timer;
    QVector <QString> BlockId, BoardId, PortId;
    QVector <QString> BoardParentId, PortParentId;
    QVector <QTreeWidgetItem*> blockItems, boardItems, portItems;
    void processData(QByteArray byteArr);
    void setWidgetTree(QString tag, QString attributes, QString id, QString parentId);
};
#endif // MAINWINDOW_H
