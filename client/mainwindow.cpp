#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->treeWidget->setColumnCount(1);
    client_socket = new QTcpSocket;
    connect(client_socket, &QTcpSocket::disconnected, client_socket, &QTcpSocket::deleteLater);
    connect(&timer, &QTimer::timeout, this, &MainWindow::sendRequest);
    connect(client_socket,& QTcpSocket::readyRead, this, &MainWindow::readData);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendRequest()
{
    client_socket->write(request);
    BlockId.clear(); BoardId.clear(); PortId.clear();
    BoardParentId.clear(); PortParentId.clear();
    blockItems.clear(); boardItems.clear(); portItems.clear();
    ui->treeWidget->clear();
}

void MainWindow::on_connectButton_clicked()
{
    client_socket->connectToHost("127.0.0.1", 8888);
    if(client_socket->waitForConnected(1000))
    {
        ui->connectionInfo->setText("Connection success");
        timer.start(ONE_MIN);
        sendRequest();
    }
    else
    {
       ui->connectionInfo->setText(client_socket->errorString());
    }
}

void MainWindow::readData()
{
    QByteArray byteArr;
    QString str;
    while(client_socket->bytesAvailable() > 0)
    {
        byteArr = client_socket->read(sizeof(char));
        int size = byteArr[0];
        byteArr.clear();
        byteArr = client_socket->read(size);
        processData(byteArr);
    }

}


void MainWindow::processData(QByteArray byteArr)
{
    QString attr(byteArr);
    QString tag;
    int startIndex;

    int i = 0;
    while(attr[i] != " ")
    {
        tag+=attr[i];
        i++;
    }

    if(tag == "block")
    {
        QString blockId;
        startIndex = 9;
        i = startIndex;
        while(attr[i] != " " )
        {
            blockId+=attr[i];
            i++;
        }
        setWidgetTree(tag, attr, blockId, "");
        BlockId.push_back(blockId);
        blockId.clear();
    }

    if(tag == "board")
    {
        QString boardId, parentId;
        startIndex = 15;
        i = startIndex;
        while(attr[i] != " " )
        {
            parentId+=attr[i];
            i++;
        }

        while(attr[i] != "=" )
        {
            i++;
        } i++;

        while(attr[i] != " " )
        {
            boardId+=attr[i];
            i++;
        }
        setWidgetTree(tag, attr, boardId, parentId);
        BoardId.push_back(boardId);
        boardId.clear();
    }

    if(tag == "port")
    {
        QString portId, parentId;
        startIndex = 14;
        i = startIndex;
        while(attr[i] != " " )
        {
            parentId+=attr[i];
            i++;
        }

        while(attr[i] != "=" )
        {
            i++;
        } i++;

        while(attr[i] != " " )
        {
            portId+=attr[i];
            i++;
        }

        setWidgetTree(tag, attr, portId, parentId);
        PortId.push_back(portId);
        portId.clear();
    }

}

void MainWindow::setWidgetTree(QString tag, QString attributes, QString id, QString parentId)
{
    if(tag == "block" && BlockId.contains(id) == false)
    {
       QTreeWidgetItem* block_item = new QTreeWidgetItem(ui->treeWidget);
       ui->treeWidget->addTopLevelItem(block_item);
       block_item->setText(0,attributes);
       blockItems.push_back(block_item);
    }

    if(tag == "board" && BoardId.contains(id) == false)
    {
        QTreeWidgetItem* board_item
                = new QTreeWidgetItem(blockItems.value(BlockId.indexOf(parentId)));
        board_item->setText(0, attributes);
        boardItems.push_back(board_item);
    }

    if(tag == "port" && PortId.contains(id) == false)
    {
        QTreeWidgetItem* port_item
                = new QTreeWidgetItem(boardItems.value(BoardId.indexOf(parentId)));
        port_item->setText(0,attributes);
        portItems.push_back(port_item);
    }
}






























