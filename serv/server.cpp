#include "server.h"



Server::Server()
{
    requestCount = 0;
    isFileChanged = false;
    dataFromClient.clear();
    filters << "*.xml";
    block_attr = {"id", "Name", "IP","BoardCount", "MtR", "MtC", "Description", "Label"};
    board_attr = {"parentID", "id", "Num", "Name", "PortCount", "IntLinks", "Algoritms"};
    port_attr = {"parentID", "id", "Num", "Media", "Signal"};
    tags = {"block", "board", "port"};
    blockTableIsCreated = false;
    boardTableIsCreated = false;
    portTableIsCreated = false;
    fsWatcher = new QFileSystemWatcher;

    QDir dir;
    xmlFileNames = dir.entryList(filters);
    for(int i=0; i < xmlFileNames.length(); i++)
    {
        qDebug() << xmlFileNames.value(i);
    }

    //Открытие бд, запись
    db = new QSqlDatabase;
    *db = QSqlDatabase::addDatabase("QSQLITE");
    db->setDatabaseName(DB_FILENAME);

    if(db->open())
    {
        qDebug() << "Database is open";
        query = new QSqlQuery(*db);
        for(int i = 0; i < xmlFileNames.length(); i++)
        {
            fsWatcher->addPath(QDir::currentPath() + "/" + xmlFileNames.value(i));
            readXML(xmlFileNames.value(i), FILL);
            blockTableIsCreated = false;
            boardTableIsCreated = false;
            portTableIsCreated = false;
        }
    }
    else
    {
        qDebug() << "Database is not open";
    }

    connect(fsWatcher, &QFileSystemWatcher::fileChanged, this, &Server::filesChanges);
    connect(this, &QTcpServer::newConnection, this, &Server::waitConnection);
    connect(this,&Server::dbReadyToSend, this, &Server::sendDB);
    connect(&timer,&QTimer::timeout, this, &Server::checkChanges);
    timer.start(ONE_MIN);
    if(this->listen(QHostAddress::Any, 8888))
        qDebug() << "Server started";
    else
        qDebug() << "Something falls";
}

void Server::checkChanges()
{
    if(isFileChanged == false)
        return;

    for(int i = 0; i < changedFiles.length(); i++)
    {
        readXML(changedFiles.value(i), UPDATE);
    }

    isFileChanged = false;
    changedFiles.clear();
}

void Server::waitConnection()
{
    server_socket = this->nextPendingConnection();
    connect(server_socket, &QTcpSocket::readyRead, this, &Server::request);
    connect(server_socket, &QTcpSocket::disconnected, server_socket, &QTcpSocket::deleteLater);
    qDebug() << "Client connected: " << server_socket->socketDescriptor();
}

void Server::request()
{
    while(server_socket->bytesAvailable() > 0)
    {
        dataFromClient.append(server_socket->readAll());
    }

    if(dataFromClient == clientRequest)
    {
        requestCount++;
        qDebug() << requestCount << " request";
        emit dbReadyToSend();
    }

    dataFromClient.clear();
}

void Server::filesChanges(QString fileName)
{
    changedFiles.append(fileName);
    isFileChanged = true;
}

void Server::sendDB()
{
    QString str;
    QByteArray byteArr;
    QVector <QString> attr;
    for(int i = 0; i < tags.length(); i++)
    {
        if(!query->exec("SELECT * FROM " + tags.value(i)))
            qDebug() << query->lastError().text();
        while(query->next())
        {
            if(tags.value(i) == "block")
                attr = block_attr;
            if(tags.value(i) == "board")
                attr = board_attr;
            if(tags.value(i) == "port")
                attr = port_attr;

            str = tags.value(i) + " ";

            for(int i = 0; i < attr.length(); i++)
            {
                str += attr.value(i) + "=" +
                        query->value(i).toString() + " ";
            }
            byteArr[0] = str.size();
            byteArr += str;
            server_socket->write(byteArr);
            byteArr.clear();
            str.clear();
        }
    }

}


void Server::readXML(QString path, bool flag)
{
    static QVector<QString> tagVector;
    QFile xmlFile(path);
    if(!xmlFile.open(QIODevice::ReadOnly))
    {
      qDebug() << "XML file is not open: " << xmlFile.errorString();
      return;
    }

    QDomDocument xmlDom;
    xmlDom.setContent(&xmlFile);
    xmlFile.close();

    QDomElement root = xmlDom.documentElement();

    /*====BLOCK===*/
    QDomNodeList block_items = root.childNodes();
    for(int i = 0; i < block_items.length(); i++)
    {
      QDomElement block_item = block_items.at(i).toElement();
      if(blockTableIsCreated == false)
      {
        createTable("block");
        blockTableIsCreated = true;
      }
      if(flag == FILL)
        fillTable(block_item, block_parent_id);
      if(flag == UPDATE)
        updateTable(block_item, block_parent_id);
      /*====BOARD====*/
      QDomNodeList board_items = block_item.childNodes();
      for(int i = 0; i < board_items.length(); i++)
      {
         QDomElement board_item = board_items.at(i).toElement();

         if(boardTableIsCreated == false)
         {
           createTable("board");
           boardTableIsCreated = true;
         }
         if (flag == FILL)
            fillTable(board_item, block_item.attribute("id"));
         if(flag == UPDATE)
            updateTable(board_item, block_item.attribute("id"));

         /*====PORT====*/
         QDomNodeList port_items = board_item.childNodes();
         for(int i = 0; i < port_items.length(); i++)
         {
            QDomElement port_item = port_items.at(i).toElement();

            if(portTableIsCreated == false)
            {
              createTable("port");
              portTableIsCreated = true;
            }
            if(flag == FILL)
                fillTable(port_item, board_item.attribute("id"));
            if(flag == UPDATE)
                updateTable(port_item, board_item.attribute("id"));
         }
      }
    }
}

void Server::createTable(QString tag)
{
    QString str;

    switch(tags.indexOf(tag))
    {
        case 0:
        {
            str = "CREATE TABLE block(id TEXT, Name TEXT, IP TEXT, "
            "BoardCount TEXT, MtR TEXT, MtC TEXT, Description TEXT, Label TEXT);";
        }break;

        case 1:
        {
            str = "CREATE TABLE board(parentID TEXT, id TEXT, Num TEXT, "
            "Name TEXT, PortCount TEXT, IntLinks TEXT, Algoritms TEXT);";
        }break;

        case 2:
        {
            str = "CREATE TABLE port(parentID TEXT, id TEXT, "
            "Num TEXT, Media TEXT, Signal TEXT);";
        }break;
    }
    query->exec(str);
}

bool Server::findId(QDomElement item)
{
    QString str = "SELECT id FROM " + item.tagName() + " WHERE "
            "id = " + item.attribute("id");
    query->exec(str);
    return query->next();
}

void Server::fillTable(QDomElement item, QString parent_id)
{
    // Если в таблице уже есть такой id - пропускаем заполнение
    if(findId(item))
        return;

    QVector <QString> attr;
    QString str;
    int start_index = 0;

    switch(tags.indexOf(item.tagName()))
    {
        case 0: attr = block_attr; break;
        case 1: attr = board_attr; break;
        case 2: attr = port_attr; break;
    }

    //insert var names
    str = "INSERT INTO " + item.tagName() + " (";
    for(int i = 0; i < attr.length(); i++)
    {
        str += attr.value(i);
        if(i != attr.length()-1)
            str += ", ";
        else
            str += ") ";
    }

    //insert values
    str += ( "VALUES (");
    if(item.tagName() != "block")
    {
        str += "'" + parent_id + "', ";
        start_index = 1;
    }
    for(int i = start_index; i < attr.length(); i++)
    {
        QString attrVal = item.attribute(attr.value(i));
        str += ( "'" + item.attribute(attr.value(i)) + "'" );
        if(i != attr.length()-1)
            str += ", ";
        else
            str += ");";
    }

    query->exec(str);
}


void Server::updateTable(QDomElement item, QString parent_id)
{
//    QVector <QString> attr;
//    QString str;
//    int start_index = 0;

//    switch(tags.indexOf(item.tagName()))
//    {
//        case 0: attr = block_attr; break;
//        case 1: attr = board_attr; break;
//        case 2: attr = port_attr; break;
//    }

//    str = "UPDATE " + item.tagName() + " SET ";
//    if(item.tagName() != "block")
//    {
//        str += "parentID = '" + parent_id + "', ";
//        start_index = 1;
//    }

//    for (int i = start_index; i < attr.length(); i++)
//    {
//        str += attr.value(i) + " = '" + item.attribute(attr.value(i))
//                + "'";
//        if(i != attr.length()-1)
//            str += ", ";
//    }

//    query->exec(str);
}































