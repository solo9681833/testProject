#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFile>
#include <QDir>
#include <QDomDocument>
#include <QtCore>
#include "QTcpServer"
#include "QTcpSocket"
#include <QSqlError>
#include <QFileSystemWatcher>
#define DB_FILENAME "database.db"

#define block_parent_id ""
#define ONE_MIN 60000
#define UPDATE 1
#define FILL 0


class Server : public QTcpServer
{
    Q_OBJECT

public:
    Server();
    QTcpSocket* server_socket;

private:
    bool isFileChanged;
    QVector <QString> changedFiles;
    QFileSystemWatcher* fsWatcher;
    const QByteArray clientRequest = "REQUEST TO SERVER";
    QStringList filters;
    QStringList xmlFileNames;
    QSqlDatabase* db;
    QSqlQuery* query;
    int requestCount;
    QByteArray dataFromClient;
    void readXML(QString path, bool flag);
    void createTable(QString tag);
    void fillTable(QDomElement item, QString parent_id);
    void updateTable(QDomElement item, QString parent_id);
    bool blockTableIsCreated, boardTableIsCreated, portTableIsCreated;
    bool findId(QDomElement item);
    QVector <QString> block_attr;
    QVector <QString> board_attr;
    QVector <QString> port_attr;
    QVector <QString> tags;
    QTimer timer;
signals:
    void dbReadyToSend();

public slots:
    void request();
    void sendDB();
    void waitConnection();
    void filesChanges(QString fileName);
    void checkChanges();
};

#endif // SERVER_H
